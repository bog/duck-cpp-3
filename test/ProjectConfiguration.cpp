#include <sstream>
#include <catch.hpp>
#include <Project.hpp>
#include <Parser.hpp>
#include <Build.hpp>

TEST_CASE("Default values", "[ProjectConfiguration]")
{
  Project p {"MyProject"};

  REQUIRE(p.cxx() == "g++");
  REQUIRE(p.flags() == "");
  REQUIRE(p.prefix() == "/usr/local");
  REQUIRE(p.objdir() == "build/obj");
  REQUIRE(p.bindir() == "build/bin");
}

TEST_CASE("Simple config", "[ProjectConfiguration]")
{
  std::stringstream source;
  source << "[Hello]" << std::endl;
  source << "@cxx abc" << std::endl;
  source << "" << std::endl;
  source << "@flags def" << std::endl;
  source << "@prefix ghi" << std::endl; 
  source << "@objdir jkl" << std::endl;
  source << "@bindir mno" << std::endl;
  
  Parser parser;
  std::unique_ptr<Build> build { parser.newBuild(source.str()) };
  REQUIRE(build->projects().size() == 1);
  Project* project = build->projects()[0].get();
  
  REQUIRE(project->name() == "Hello");
  REQUIRE(project->cxx() == "abc");
  REQUIRE(project->flags() == "def");
  REQUIRE(project->prefix() == "ghi");
  REQUIRE(project->objdir() == "jkl");
  REQUIRE(project->bindir() == "mno");
}

TEST_CASE("Two configs", "[ProjectConfiguration]")
{
  std::stringstream source;
  source << "[Hello]" << std::endl;
  source << "@cxx abc" << std::endl;
  source << "" << std::endl;
  source << "@flags def" << std::endl;
  source << "@prefix ghi" << std::endl; 
  source << "@objdir jkl" << std::endl;
  source << "@bindir mno" << std::endl;

  source << "[World]" << std::endl;
  source << "@cxx pqr" << std::endl;
  source << "" << std::endl;
  source << "@flags stu" << std::endl;
  source << "@prefix vwx" << std::endl; 
  source << "@objdir yzy" << std::endl;
  source << "@bindir xwv" << std::endl;

  Parser parser;
  std::unique_ptr<Build> build { parser.newBuild(source.str()) };
  REQUIRE(build->projects().size() == 2);
  
  Project* project0 = build->projects()[0].get();
  Project* project1 = build->projects()[1].get();
  
  REQUIRE(project0->name() == "Hello");
  REQUIRE(project0->cxx() == "abc");
  REQUIRE(project0->flags() == "def");
  REQUIRE(project0->prefix() == "ghi");
  REQUIRE(project0->objdir() == "jkl");
  REQUIRE(project0->bindir() == "mno");

  REQUIRE(project1->name() == "World");
  REQUIRE(project1->cxx() == "pqr");
  REQUIRE(project1->flags() == "stu");
  REQUIRE(project1->prefix() == "vwx");
  REQUIRE(project1->objdir() == "yzy");
  REQUIRE(project1->bindir() == "xwv");
}
