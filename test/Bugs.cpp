#include <catch.hpp>
#include <sstream>
#include <Parser.hpp>
#include <Build.hpp>
#include <Project.hpp>

TEST_CASE("Flag no set", "[bug]")
{
  std::stringstream source;
  source << "[Project_A]" << std::endl;
  source << "> hello.cpp hello.hpp" << std::endl;
  source << "> world.cpp world.hpp" << std::endl;
  source << "< hello.exe hello.elf" << std::endl;

  source << "[Project_B]" << std::endl;
  source << "> hola.cpp hola.hpp" << std::endl;
  source << "> mundo.cpp mundo.hpp" << std::endl;
  source << "< hola.exe hola.elf" << std::endl;

  source << "[Project_C]" << std::endl;
  source << "@flags -Wall -std=c++17" << std::endl;
  source << "> hola.cpp" << std::endl;
  
  Parser parser;
  std::unique_ptr<Build> build { parser.newBuild(source.str()) };

  REQUIRE(build->projects().size() == 3);
  REQUIRE(build->projects()[2]->flags() == "-Wall -std=c++17");  
}

TEST_CASE("SeveralExtensions", "[bug]")
{
  std::stringstream source;
  source << "[Project_A]" << std::endl;
  source << "> hello.0.1.pc" << std::endl;
  source << "< hello.elf" << std::endl;
  
  Parser parser;
  std::unique_ptr<Build> build { parser.newBuild(source.str()) };

  REQUIRE(build->projects().size() == 1);
  REQUIRE(build->projects()[0]->importLibs().size() == 1);
  REQUIRE(build->projects()[0]->importLibs()[0] == "hello.0.1.pc");  
}
