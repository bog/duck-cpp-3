#include <sstream>
#include <catch.hpp>
#include <Parser.hpp>
#include <Build.hpp>
#include <Project.hpp>

/*****************/
/* Nominal cases */
/*****************/

TEST_CASE("Simple multi lines import file", "[ImportFile]")
{
  std::stringstream source;
  source << "[MyProject]" << std::endl;
  source << "> we.cpp" << std::endl;
  source << "> are.cxx" << std::endl;
  source << "> the.cc" << std::endl;
  source << "> champions.c" << std::endl;

  Parser parser;
  std::unique_ptr<Build> build { parser.newBuild(source.str()) };
    
  REQUIRE(build->projects().size() == 1);
  Project* p = build->projects()[0].get();

  REQUIRE(p->importSources().size() == 4);
  REQUIRE(p->importSources()[0] == "we.cpp");
  REQUIRE(p->importSources()[1] == "are.cxx");
  REQUIRE(p->importSources()[2] == "the.cc");
  REQUIRE(p->importSources()[3] == "champions.c");
}

TEST_CASE("Simple one line import file", "[ImportFile]")
{
  std::stringstream source;
  source << "[MyProject]" << std::endl;
  source << "> i.cpp really.cxx love.cc pizza.c" << std::endl;

  Parser parser;
  std::unique_ptr<Build> build { parser.newBuild(source.str()) };
    
  REQUIRE(build->projects().size() == 1);
  Project* p = build->projects()[0].get();

  REQUIRE(p->importSources().size() == 4);
  REQUIRE(p->importSources()[0] == "i.cpp");
  REQUIRE(p->importSources()[1] == "really.cxx");
  REQUIRE(p->importSources()[2] == "love.cc");
  REQUIRE(p->importSources()[3] == "pizza.c");
}


TEST_CASE("Simple one line and multi lines import file", "[ImportFile]")
{
  std::stringstream source;
  source << "[MyProject]" << std::endl;
  source << "> a.cpp b.cxx" << std::endl;
  source << "> c.cc d.c" << std::endl;

  Parser parser;
  std::unique_ptr<Build> build { parser.newBuild(source.str()) };
    
  REQUIRE(build->projects().size() == 1);
  Project* p = build->projects()[0].get();

  REQUIRE(p->importSources().size() == 4);
  REQUIRE(p->importSources()[0] == "a.cpp");
  REQUIRE(p->importSources()[1] == "b.cxx");
  REQUIRE(p->importSources()[2] == "c.cc");
  REQUIRE(p->importSources()[3] == "d.c");
}
TEST_CASE("Two projects one line import file", "[ImportFile]")
{
  std::stringstream source;
  source << "[MyProject0]" << std::endl;
  source << "> a.cpp b.cxx c.cc d.c" << std::endl;
  source << "[MyProject1]" << std::endl;
  source << "> e.cpp f.cxx g.cc h.c" << std::endl;

  Parser parser;
  std::unique_ptr<Build> build { parser.newBuild(source.str()) };
    
  REQUIRE(build->projects().size() == 2);
  
  Project* p0 = build->projects()[0].get();
  REQUIRE(p0->importSources().size() == 4);
  REQUIRE(p0->importSources()[0] == "a.cpp");
  REQUIRE(p0->importSources()[1] == "b.cxx");
  REQUIRE(p0->importSources()[2] == "c.cc");
  REQUIRE(p0->importSources()[3] == "d.c");

  Project* p1 = build->projects()[1].get();
  REQUIRE(p1->importSources().size() == 4);
  REQUIRE(p1->importSources()[0] == "e.cpp");
  REQUIRE(p1->importSources()[1] == "f.cxx");
  REQUIRE(p1->importSources()[2] == "g.cc");
  REQUIRE(p1->importSources()[3] == "h.c");
}


/***************/
/* Error cases */
/***************/

TEST_CASE("Ignore empty import.", "[ImportFile]")
{
  std::stringstream source;
  source << "[MyProject]" << std::endl;
  source << "> this.cpp" << std::endl;
  source << "> " << std::endl;
  source << "> is.cc" << std::endl;
  source << "> sparta.c" << std::endl;

  Parser parser;
  std::unique_ptr<Build> build { parser.newBuild(source.str()) };
    
  REQUIRE(build->projects().size() == 1);
  Project* p = build->projects()[0].get();

  REQUIRE(p->importSources().size() == 3);
  REQUIRE(p->importSources()[0] == "this.cpp");
  REQUIRE(p->importSources()[1] == "is.cc");
  REQUIRE(p->importSources()[2] == "sparta.c");
}

TEST_CASE("Ignore additional spaces import file", "[ImportFile]")
{
  std::stringstream source;
  source << "[MyProject]" << std::endl;
  source << "   > a.cpp b.cxx        " << std::endl;
  source << ">    c.cc           d.c" << std::endl;

  Parser parser;
  std::unique_ptr<Build> build { parser.newBuild(source.str()) };
    
  REQUIRE(build->projects().size() == 1);
  Project* p = build->projects()[0].get();

  REQUIRE(p->importSources().size() == 4);
  REQUIRE(p->importSources()[0] == "a.cpp");
  REQUIRE(p->importSources()[1] == "b.cxx");
  REQUIRE(p->importSources()[2] == "c.cc");
  REQUIRE(p->importSources()[3] == "d.c");
}

TEST_CASE("Import simple header", "[ImportFile]")
{
  std::stringstream source;
  source << "[Project_0]" << std::endl << std::endl;
  source << "> hello.c hello.h" << std::endl;
  source << "> world.c" << std::endl;
  source << "> world.h" << std::endl;
  source << "[Project_1]" << std::endl << std::endl;
  source << "> pizza.c pizza.h" << std::endl;
  source << "> kebab.c" << std::endl;
  source << "> kebab.h" << std::endl;
  
  Parser parser;
  std::unique_ptr<Build> build { parser.newBuild(source.str()) };

  REQUIRE(build->projects().size() == 2);
  
  Project* p0 = build->projects()[0].get();
  REQUIRE(p0->importHeaders().size() == 2);
  REQUIRE(p0->importHeaders()[0] == "hello.h");
  REQUIRE(p0->importHeaders()[1] == "world.h");
  
  Project* p1 = build->projects()[1].get();
  REQUIRE(p1->importHeaders().size() == 2);
  REQUIRE(p1->importHeaders()[0] == "pizza.h");
  REQUIRE(p1->importHeaders()[1] == "kebab.h");
}


TEST_CASE("Import header with different extension", "[ImportFile]")
{
  std::stringstream source;
  source << "[Project_0]" << std::endl << std::endl;
  source << "> a.c   hello.h d.cxx " << std::endl;
  source << "> b.cpp  world.hpp f.cc " << std::endl;
  
  Parser parser;
  std::unique_ptr<Build> build { parser.newBuild(source.str()) };

  REQUIRE(build->projects().size() == 1);
  
  Project* p0 = build->projects()[0].get();
  REQUIRE(p0->importHeaders().size() == 2);
  REQUIRE(p0->importHeaders()[0] == "hello.h");
  REQUIRE(p0->importHeaders()[1] == "world.hpp");
}


TEST_CASE("Import libraries", "[ImportFile]")
{
  std::stringstream source;
  source << "[Project_0]" << std::endl;
  source << "> a.h a.cpp lib0.a" << std::endl;
  source << "   > b.cpp  lib1.so world.hpp f.cc " << std::endl;
  source << "> d.cpp  lib2.pc              lib3.dll world.hpp f.cc " << std::endl;
  source << "[Project_1]" << std::endl;
  source << "> hello.a hello.so hello.dll hello.pc" << std::endl;
  
  Parser parser;
  std::unique_ptr<Build> build { parser.newBuild(source.str()) };

  REQUIRE(build->projects().size() == 2);
  
  Project* p0 = build->projects()[0].get();
  REQUIRE(p0->importLibs().size() == 4);
  REQUIRE(p0->importLibs()[0] == "lib0.a");
  REQUIRE(p0->importLibs()[1] == "lib1.so");
  REQUIRE(p0->importLibs()[2] == "lib2.pc");
  REQUIRE(p0->importLibs()[3] == "lib3.dll");

  Project* p1 = build->projects()[1].get();
  REQUIRE(p1->importLibs().size() == 4);
  REQUIRE(p1->importLibs()[0] == "hello.a");
  REQUIRE(p1->importLibs()[1] == "hello.so");
  REQUIRE(p1->importLibs()[2] == "hello.dll");
  REQUIRE(p1->importLibs()[3] == "hello.pc");
}
