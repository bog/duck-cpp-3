#include <catch.hpp>
#include <sstream>
#include <memory>
#include <Parser.hpp>
#include <Build.hpp>
#include <Project.hpp>


TEST_CASE("Create three projects", "[Project]")
{
  std::stringstream source;
  source << " [Project_A]" << std::endl;
  source << " " << std::endl;
  source << "  [       Project_B]  " << std::endl;
  source << "  [Project_C         ]" << std::endl;
  
  Parser parser;
  std::unique_ptr<Build> build = std::unique_ptr<Build>( parser.newBuild(source.str()) );
  
  REQUIRE(build->projects().size() == 3);
  REQUIRE(build->projects()[0]->name() == "Project_A");
  REQUIRE(build->projects()[1]->name() == "Project_B");
  REQUIRE(build->projects()[2]->name() == "Project_C");
}


