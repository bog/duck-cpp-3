#include <sstream>
#include <catch.hpp>
#include <Parser.hpp>
#include <Build.hpp>
#include <Project.hpp>

TEST_CASE("Export simples bins", "[ExportFile]")
{
  std::stringstream source;
  source << "[HelloWorld0]"<< std::endl;
  source << "< prog0.elf" << std::endl;
  source << "            <            prog1.elf   " << std::endl;
  source << "< prog2.elf prog3.elf" << std::endl;
  source << "< win0.exe prog4.elf win1.exe prog5.elf" << std::endl;
  source << " <   win2.exe   prog6.elf  " << std::endl;
  
  Parser parser;
  std::unique_ptr<Build> build { parser.newBuild(source.str()) };

  REQUIRE(build->projects().size() == 1);

  Project* p = build->projects()[0].get();

  REQUIRE(p->exportBins().size() == 10);
  REQUIRE(p->exportBins()[0] == "prog0.elf");
  REQUIRE(p->exportBins()[1] == "prog1.elf");
  REQUIRE(p->exportBins()[2] == "prog2.elf");
  REQUIRE(p->exportBins()[3] == "prog3.elf");
  REQUIRE(p->exportBins()[4] == "win0.exe");
  REQUIRE(p->exportBins()[5] == "prog4.elf");
  REQUIRE(p->exportBins()[6] == "win1.exe");
  REQUIRE(p->exportBins()[7] == "prog5.elf");
  REQUIRE(p->exportBins()[8] == "win2.exe");
  REQUIRE(p->exportBins()[9] == "prog6.elf");
}

TEST_CASE("Export bins in two projects", "[ExportFile]")
{
  std::stringstream source;
  source << "[Elephant]"<< std::endl;
  source << "< hello.elf world.exe"<< std::endl;
  source << "[Mandarine]"<< std::endl;
  source << "< peter.exe pan.elf"<< std::endl;
  
  Parser parser;
  std::unique_ptr<Build> build { parser.newBuild(source.str()) };

  REQUIRE(build->projects().size() == 2);

  Project* p0 = build->projects()[0].get();
  REQUIRE(p0->exportBins().size() == 2);
  REQUIRE(p0->exportBins()[0] == "hello.elf");
  REQUIRE(p0->exportBins()[1] == "world.exe");

  Project* p1 = build->projects()[1].get();
  REQUIRE(p1->exportBins().size() == 2);
  REQUIRE(p1->exportBins()[0] == "peter.exe");
  REQUIRE(p1->exportBins()[1] == "pan.elf");
}
