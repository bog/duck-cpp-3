#include <iostream>
#include <sstream>
#include <regex>
#include <functional>
#include <filesystem>
#include <MakefileGenerator.hpp>
#include <Build.hpp>
#include <Project.hpp>

MakefileGenerator::MakefileGenerator(Build const& build)
  : m_build { build }
{
}

std::string MakefileGenerator::generate()
{
  std::stringstream makefile;

  /// ALL  
  makefile << "all: create_dirs ";
  for (auto const& project : m_build.projects())
    {
      // Bins
      for (auto const& bin : project->exportBins())
	{
	  if ( project->bindir().empty() )
	    {
	      makefile << bin << " ";
	    }
	  else
	    {
	      makefile << project->bindir() << "/" << bin << " ";
	      createIfNotExist(project->bindir() + "/" + std::filesystem::path{bin}.parent_path().c_str());
	    }
	}
      
      // Libs
      for (auto const& lib : project->exportLibs())
	{
	  makefile << project->libdir() << "/" << lib << " ";
	  createIfNotExist(project->libdir() + "/" + std::filesystem::path{lib}.parent_path().c_str());
	}
    }    
  
  makefile << std::endl << std::endl;
  
  ///
  // Create dirs
  createDirs(makefile);
  
  for (auto const& project : m_build.projects())
    {
      // BEGIN PROJECT
      makefile << "### " << project->name() << std::endl << std::endl;

      /// FOR EACH EXPORT
      for (auto const& bin : project->exportBins())
	{
	  std::string bin_ext = std::regex_replace(bin, std::regex {".*\\.(.*)"}, "$1");
	  std::string bin_name = std::regex_replace(bin, std::regex {"(.*)\\..*"}, "$1");
	  std::string cxx = project->cxx();

	  if (bin_ext == "exe")
	    {
	      cxx = "x86_64-w64-mingw32-" + cxx;
	    }

	  makefile << "## " << bin << std::endl;

	  /// OBJ GENERATION
	  objGeneration(project.get(), bin_ext, makefile, cxx, "");
	  
	  makefile << std::endl;
	  
	  /// BIN GENERATION
	  
	  makefile << std::endl;	  
	  makefile << project->bindir()
		   << "/" << bin << ": ";
	  createIfNotExist(project->bindir() + "/" + std::filesystem::path(bin).parent_path().c_str());
	  
	  for (auto const& source : project->importSources())
	    {
	      std::string object = std::regex_replace(source, std::regex {"(.*)\\..*"}, "$1.o");
	      makefile << project->objdir() << "/" << project->name() << "/" << bin_ext << "/" << object << " ";
	      createIfNotExist(project->objdir() + "/" + project->name() + "/" + bin_ext + "/"
			       + std::filesystem::path(object).parent_path().c_str());
	    }
	  
	  makefile << std::endl;
	  makefile << "\t" << cxx << " $^ " << project->flags();
	  for (auto const& f : project->importLibs())
	    {
	      std::string name = std::regex_replace(f, std::regex {"(.*)\\..*"}, "$1");
	      std::string ext = std::regex_replace(f, std::regex {".*\\.(.*)"}, "$1");

	      if (ext == "a" || ext == "so" || ext == "dll")
		{
		  makefile << " -l" << name;
		}
	      if (ext == "pc")
		{
		  makefile << " $(shell pkg-config --libs " + name + ")";
		}
	    }
	  makefile << " -o $@" << std::endl;
	  
	  ///
	}
      /// FOR EACH EXPORT LIBS
      for (auto const& lib : project->exportLibs())
	{
	  std::string lib_ext = std::regex_replace(lib, std::regex {".*\\.(.*)"}, "$1");
	  std::string lib_name = std::regex_replace(lib, std::regex {"(.*)\\..*"}, "$1");
	  std::string cxx = project->cxx();
	  
	  objGeneration(project.get(), lib_ext, makefile, cxx, "-fpic");
		  
	  makefile << project->libdir() << "/" << lib << ": ";
	  for (auto const& source : project->importSources())
	    {
	      std::string source_obj = std::regex_replace(source, std::regex {"(.*)\\..*"}, "$1.o");	      
	      
	      makefile << project->objdir() << "/" << project->name() << "/" << lib_ext << "/" << source_obj << " ";
	      createIfNotExist(project->objdir() + "/" + project->name() + "/" + lib_ext + "/"
			       + std::filesystem::path(source_obj).parent_path().c_str());
	    }
	  
	  makefile << std::endl;

	  if (lib_ext == "a")
	    {
	      makefile << "\t" << "ar q $@ $^";
	    }

	  if (lib_ext == "so" || lib_ext == "dll")
	    {
	      makefile << "\t" << project->cxx() << " -fpic -shared  $^ " << project->flags() << " -o $@" << std::endl;
	    }
	}
      ///
      ///

      // END PROJECT                  
      makefile << std::endl << std::endl;
    }

  makefile << "u: update" << std::endl;
  makefile << "\t" << std::endl;

  makefile << "update: " << std::endl;
  makefile << "\tmake mrproper" << std::endl;
  makefile << "\tduckcpp > Makefile" << std::endl;
  makefile << "\tmake" << std::endl;
  
  makefile << ".PHONY: clean mrproper install uninstall create_dirs u update" << std::endl;
  
  cleanGeneration(makefile);
  mrproperGeneration(makefile);
  installGeneration(makefile);
  uninstallGeneration(makefile);
  
  return makefile.str();
}

void MakefileGenerator::createIfNotExist(std::string const& path)
{
  // if ( !std::filesystem::exists(path) )
  //   {
  //     std::filesystem::create_directories(path);
  //   }
}

void MakefileGenerator::createDirs(std::stringstream& makefile)
{
  makefile << "create_dirs:" << std::endl;

  for (auto const& project : m_build.projects())
    {
      makefile << "\t" << "@mkdir -p " << project->libdir()  << std::endl;
      makefile << "\t" << "@mkdir -p " << project->bindir()  << std::endl;
      makefile << "\t" << "@mkdir -p " << project->objdir()  << std::endl;
  
      for (auto const& bin : project->exportBins())
	{
	  for (auto const& src : project->importSources())
	    {
	      std::string object = std::regex_replace(src, std::regex {"(.*)\\..*"}, "$1.o");
	      std::string ext = std::regex_replace(bin, std::regex {".*\\.(.*)"}, "$1");
	      std::string bin_path = std::filesystem::path { object }.parent_path().c_str();
	  
	      makefile << "\t"
		       << "@mkdir -p "
		       << project->objdir()
		       << "/"
		       << project->name()
		       << "/"
		       << ext
		       << "/"
		       << bin_path
		       << std::endl;
	    }
	}

      for (auto const& lib : project->exportLibs())
	{
	  for (auto const& src : project->importSources())
	    {
	      std::string object = std::regex_replace(src, std::regex {"(.*)\\..*"}, "$1.o");
	      std::string ext = std::regex_replace(lib, std::regex {".*\\.(.*)"}, "$1");
	      std::string lib_path = std::filesystem::path { object }.parent_path().c_str();
	  
	      makefile << "\t"
		       << "@mkdir -p "
		       << project->objdir()
		       << "/"
		       << project->name()
		       << "/"
		       << ext
		       << "/"
		       << lib_path
		       << std::endl;
	    }
	}

      for (auto const& dir : project->exportDirs())
	{
	  makefile << "\t" << "@mkdir -p " << dir << "/bin" << std::endl;
	  makefile << "\t" << "@mkdir -p " << dir << "/lib" << std::endl;
	  makefile << "\t" << "@mkdir -p " << dir << "/include" << std::endl;
	}
    }
}
 
void MakefileGenerator::objGeneration(Project* project,
				      std::string const& bin_ext,
				      std::stringstream& makefile,
				      std::string const& cxx,
				      std::string const& libflag)
{
  for (auto const& import : project->importSources())
    {
      std::string object = std::regex_replace(import, std::regex {"(.*)\\..*"}, "$1.o");

      auto headers_if_exist = [&](std::string const& ext){
				std::string h
				  = std::regex_replace(import, std::regex {"(.*)\\..*"},
						       "$1." + ext);
			    
				auto itr = std::find(project->importHeaders().begin(),
						     project->importHeaders().end(), h);
			    
				if ( itr != project->importHeaders().end() )
				  {
				    return " " + h;
				  }
				      
				return std::string{};
			      };
      
      makefile << project->objdir()
	       << "/" << project->name()
	       << "/" << bin_ext
	       << "/" << object << ": " << import
	       << headers_if_exist("h") 
	       << headers_if_exist("hpp")
	       << std::endl;

      createIfNotExist(project->objdir() +
		       "/" +
		       project->name() +
		       "/" +
		       bin_ext +
		       "/" +
		       std::filesystem::path {object}.parent_path().c_str());
      makefile << "\t" << cxx << " -c ";

      
      for (auto const& lib : project->importLibs())
	{
	  if (std::regex_match(lib, std::regex {"^.*\\.pc$"}))
	    {
	      makefile << "$(shell pkg-config --cflags "<< std::regex_replace(lib, std::regex {"^(.*).pc$"}, "$1") <<") ";
	    }
	}

      makefile << "$< " << project->flags() << libflag << " -o $@" << std::endl << std::endl;
    }

}

void MakefileGenerator::installGeneration(std::stringstream& makefile)
{
  makefile << "install: ";
  for (auto const& project : m_build.projects())
    {
      for (auto const& savedir : project->exportDirs())
	{
	  createIfNotExist(savedir + "/bin/");
	  createIfNotExist(savedir + "/lib/");
	  
	  for (auto const& bin : project->exportBins())
	    {
	      makefile << project->bindir() << "/" << bin << " ";
	    }
	  for (auto const& lib : project->exportLibs())
	    {
	      makefile << project->libdir() << "/" << lib << " ";
	    }
	}
    }
  
  makefile << std::endl;
  
  for (auto const& project : m_build.projects())
    {
      for (auto const& savedir : project->exportDirs())
	{	  
	  for (auto const& bin : project->exportBins())
	    {
	      std::string path = project->bindir() + "/" + bin + " " + savedir + "/bin/";
	      makefile << "\tcp -Rv " << path  << std::endl;
	    }

	  for (auto const& lib : project->exportLibs())
	    {
	      std::string path = project->libdir() + "/" + lib + " " + savedir + "/lib/";
	      makefile << "\tcp -Rv " << path << std::endl;
	    }

	  for (auto const& header : project->importHeaders())
	    {
	      std::string path = header + " " + savedir + "/include/";
	      makefile << "\tcp -Rv " << path << std::endl;
	    }
	}
    }
}

void MakefileGenerator::uninstallGeneration(std::stringstream& makefile)
{
  makefile << "uninstall: " << std::endl;
  
  for (auto const& project : m_build.projects())
    {
      for (auto const& savedir : project->exportDirs())
	{
	  for (auto const& bin : project->exportBins())
	    {
	      makefile << "\trm -v " << savedir << "/bin/" << bin  << std::endl;
	    }

	  for (auto const& lib : project->exportLibs())
	    {
	      makefile << "\trm -v " << savedir << "/lib/" << lib << std::endl;
	    }

	  for (auto const& header : project->importHeaders())
	    {
	      makefile << "\trm -v " << savedir << "/include/"
		       << std::regex_replace(header, std::regex {".*/(.*)$"}, "$1") << std::endl;
	    }	  
	}
    }
}

void MakefileGenerator::mrproperGeneration(std::stringstream& makefile)
{
  makefile << "mrproper: clean" << std::endl;
  for (auto const& project : m_build.projects())
    {
      for (auto const& bin : project->exportBins())
	{
	  makefile << "\trm -f " << project->bindir() << "/" << bin << std::endl;
	}

      for (auto const& lib : project->exportLibs())
	{
	  makefile << "\trm -f " << project->libdir() << "/" << lib << std::endl;
	}
    }
}

void MakefileGenerator::cleanGeneration(std::stringstream& makefile)
{
  makefile << "clean: " << std::endl;
  for (auto const& project : m_build.projects())
    {
      for (auto const& bin : project->exportBins())
	{
	  cleanDirectory(project.get(), makefile, bin);
	}

      for (auto const& lib : project->exportLibs())
	{
	  cleanDirectory(project.get(), makefile, lib);
	}

    }
}

void MakefileGenerator::cleanDirectory(Project* project, std::stringstream& makefile, std::string const& path)
{
  std::string ext = std::regex_replace(path, std::regex {".*\\.(.*)"}, "$1");
	  
  for (auto const& e : project->importSources())
    {
      std::string object = std::regex_replace(e, std::regex {"(.*)\\..*"}, "$1.o");
      std::string obj = project->objdir() + "/" + project->name() + "/" + ext + "/" + object;
      makefile << "\trm -f " + obj  << std::endl;
    }
}
