#include <cassert>
#include <iostream>
#include <fstream>
#include <sstream>
#include <cstring>
#include <memory>
#include <Parser.hpp>
#include <Build.hpp>
#include <Project.hpp>
#include <MakefileGenerator.hpp>

#define VERSION "0.0"

int main(int argc, char** argv)
{

  std::stringstream source;

  if (argc > 1)
    {
      if ( strcmp(argv[1], "--help") == 0 )
	{
	  std::cout << "Usage:" << std::endl;
	  std::cout << "\tduckcpp" << std::endl;
	  std::cout << "\tduckcpp <filename>" << std::endl;
	  std::cout << "\tduckcpp --help" << std::endl;
	  std::cout << "\tduckcpp --version" << std::endl;
	  exit(0);
	}

      else if ( strcmp(argv[1], "--version") == 0 )
	{
	  std::cout << "DuckC(pp)" << std::endl;
	  std::cout << "\tv" VERSION << std::endl;
	  exit(0);
	}
      else
	{
	  std::ifstream file {argv[1]};

	  if (file)
	    {
	  std::string line = "";
      
	  while (std::getline(file, line))
	    {
	      source << line << std::endl;
	    }

	  file.close();
	    }
	  else
	    {
	      std::cerr << "File " << argv[1] << " does not exist." << std::endl;
	      exit(-1);
	    }
	}
    }
  else
    {
      // Default build file.
      std::ifstream file { "build.duck" };
      
      if (file)
	{
	  std::string line = "";
      
	  while (std::getline(file, line))
	    {
	      source << line << std::endl;
	    }

	  file.close();
	}
      // Error
      else
	{
	  exit(-1);
	}
    }
  
  Parser parser;
  std::unique_ptr<Build> build { parser.newBuild(source.str()) };
  MakefileGenerator makefile { *build.get() };

  std::cout << makefile.generate() << std::endl;
  
  return 0;
}
