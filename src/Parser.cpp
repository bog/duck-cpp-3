#include <cassert>
#include <unordered_map>
#include <iostream>
#include <filesystem>
#include <regex>
#include <Parser.hpp>
#include <Build.hpp>
#include <Project.hpp>

Parser::Parser()
  : m_current_project { nullptr }
  , m_name_pattern { "[\\._\\-a-zA-Z0-9/]+" }
  , m_flag_pattern { ".*?" }
  , m_sources_pattern {"(?:cpp|cxx|cc|c)"}
  , m_headers_pattern {"(?:hpp|h)"}
  , m_libs_pattern {"(?:a|so|dll|pc)"}
  , m_bins_pattern {"(?:exe|elf)"}
{
}

Build* Parser::newBuild(std::string const& source)
{
  Build* build = new Build;
  
  std::string line = "";
  std::smatch match;

  std::string const sources_or_headers_pattern = "(?:" + m_sources_pattern + "|" + m_headers_pattern + ")";
  std::string const sources_or_headers_or_libs_pattern = "(?:" + m_libs_pattern + "|" + sources_or_headers_pattern + ")";
  
  for (size_t i=0; i<source.size(); i++)
    {
      char c = source[i];

      if (c != '\n')
	{
	  line += c;
	}
      else
	{
	  // Project line 
	  if ( std::regex_match(line, std::regex {"\\s*\\[.*\\]\\s*"}) )
	    {
	      line = std::regex_replace(line, std::regex {"\\s"}, "");
	      line = std::regex_replace(line, std::regex {"\\[("+m_name_pattern+")\\]"}, "$1");

	      if ( !line.empty() )
		{
		  m_current_project = new Project {line};
		  build->addNewProject(m_current_project);
		}	     
	    }
	  // Config line
	  else if ( std::regex_match(line, match, std::regex {"@("+m_name_pattern+")\\s*("+m_flag_pattern+"+)"}) )
	    {
	      assert(m_current_project);
	      
	      if (match[1] == "cxx") { m_current_project->m_cxx = match[2]; }
	      if (match[1] == "flags") { m_current_project->m_flags = match[2]; }
	      if (match[1] == "prefix") { m_current_project->m_prefix = match[2]; }
	      if (match[1] == "objdir") { m_current_project->m_objdir = match[2]; }
	      if (match[1] == "bindir") { m_current_project->m_bindir = match[2]; }
	    }
	  // Import source headers or libs.
	  else if ( std::regex_match(line,
	    			     std::regex {"^\\s*>\\s*(\\s*" +
						   m_name_pattern +
						   "\\." + sources_or_headers_or_libs_pattern +
						   "\\s*)*?\\s*$"}))
	    {
	      assert(m_current_project);
	      
	      std::string str = line;
	      
	      while ( std::regex_search(str, match,  std::regex {m_name_pattern +
								   "\\." +
								   sources_or_headers_or_libs_pattern}) )
		{
		  for (auto const& e : match)
		    {
		      std::string ext = std::regex_replace(e.str(), std::regex {".*\\.(.*)"}, "$1");

		      if (std::regex_match(ext, std::regex {"^" + m_sources_pattern + "$"}))
			{
			  m_current_project->m_import_sources.push_back(e);
			}

		      if (std::regex_match(ext, std::regex {"^" + m_headers_pattern + "$"}))
			{
			  m_current_project->m_import_headers.push_back(e);
			}

		      if (std::regex_match(ext, std::regex {"^" + m_libs_pattern + "$"}))
			{
			  m_current_project->m_import_libs.push_back(e);
			}
		    }
		  
		  str = match.suffix().str();
		}	     
	    }
	  // Import dir.
	  else if ( std::regex_match(line,
				     match,
	    			     std::regex {"^\\s*>\\s*(\\s*" +
						   m_name_pattern +						   
						   "\\s*)*?\\s*$"}))
	    {
	      search(match[1]);
	    }
	  else if ( std::regex_match(line,
				     match,
	    			     std::regex {"^\\s*>>\\s*(\\s*" +
						   m_name_pattern +						   
						   "\\s*)*?\\s*$"}))
	    {
	      searchRec(match[1]);
	    }
	  // Export
	  else if ( std::regex_match(line, std::regex {"^\\s*<\\s*("+m_name_pattern+"\\." + m_bins_pattern + "\\s*)+\\s**$"}) )
	    {
	      std::string str = line;

	      while ( std::regex_search(str, match, std::regex {m_name_pattern + "\\." + m_bins_pattern}) )
		{
		  m_current_project->m_export_bins.push_back(match[0]);
		  str = match.suffix();
		}
	    }
	  // Export Lib
	  else if ( std::regex_match(line, std::regex {"^\\s*<\\s*("+m_name_pattern+"\\." + m_libs_pattern + "\\s*)+\\s**$"}) )
	    {
	      std::string str = line;

	      while ( std::regex_search(str, match, std::regex {m_name_pattern + "\\." + m_libs_pattern}) )
		{
		  m_current_project->m_export_libs.push_back(match[0]);
		  str = match.suffix();
		}
	    }
	  // Export project
	  else if  ( std::regex_match(line, match, std::regex {"^\\s*<\\s*(" + m_name_pattern + ")\\s*$"}) )
	    {
	      std::string dir = match[1];
	      m_current_project->m_export_dirs.push_back(dir);
	    }
	  else
	    {
	      if (!line.empty())
		{
		  std::cerr << "W: \"" << line <<"\" is not a valid command." << std::endl;
		}
	    }
	  
	  line = "";
	}
    }

  checkBuild(*build);
  
  return build;
}

void Parser::checkBuild(Build& build)
{
  /// Check projects name
  {
    std::unordered_map<std::string, int> projects;
  
    for (auto const& project : build.projects())
      {
	if (projects.find(project->name()) == projects.end())
	  {
	    projects.insert({project->name(), 1});
	  }
	else
	  {
	    projects[project->name()]++;
	  }
      }

    for (auto const& p : projects)
      {
	if (p.second > 1)
	  {
	    std::cerr<<"W : multiples definitions of " << p.first << std::endl;
	  }
      }
  }
}

void Parser::searchRec(std::string const& search_path)
{
  std::filesystem::path path { search_path };

  if (std::regex_search(search_path, std::regex {".*~$"}))
    {
      return;
    }
  
  for (auto const& p : std::filesystem::directory_iterator {path})
    {
      std::string ext = std::regex_replace(p.path().c_str(), std::regex {".*\\.(.*)"}, "$1");

      if (std::regex_match(ext, std::regex {m_sources_pattern}))
	{
	  m_current_project->m_import_sources.push_back(p.path().c_str());
	}
		    
      else if (std::regex_match(ext, std::regex {m_headers_pattern}))
	{
	  m_current_project->m_import_headers.push_back(p.path().c_str());
	}
		  
      else if (std::regex_match(ext, std::regex {m_libs_pattern}))
	{
	  m_current_project->m_import_libs.push_back(p.path().c_str());
	}
      else
	{
	  searchRec(p.path().c_str());
	}
    } 
}

void Parser::search(std::string const& search_path)
{
  std::filesystem::path path { search_path };
	      
  for (auto const& p : std::filesystem::directory_iterator {path})
    {
      std::string ext = std::regex_replace(p.path().c_str(), std::regex {".*\\.(.*)"}, "$1");

      if (std::regex_match(ext, std::regex {m_sources_pattern}))
	{
	  m_current_project->m_import_sources.push_back(p.path().c_str());
	}
		    
      else if (std::regex_match(ext, std::regex {m_headers_pattern}))
	{
	  m_current_project->m_import_headers.push_back(p.path().c_str());
	}
		  
      else if (std::regex_match(ext, std::regex {m_libs_pattern}))
	{
	  m_current_project->m_import_libs.push_back(p.path().c_str());
	}
    } 
}
