#ifndef PARSER_HPP
#define PARSER_HPP
#include <string>

class Build;
class Project;

class Parser
{
public:
  Parser();
  Build* newBuild(std::string const& source);
private:
  Project* m_current_project;
  std::string const m_name_pattern;
  std::string const m_flag_pattern;
  std::string const m_sources_pattern;
  std::string const m_headers_pattern;
  std::string const m_libs_pattern;
  std::string const m_bins_pattern;

  void checkBuild(Build& build);
  void searchRec(std::string const& search_path);
  void search(std::string const& search_path);
};

#endif
