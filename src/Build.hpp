#ifndef BUILD_HPP
#define BUILD_HPP
#include <memory>
#include <vector>

class Project;

class Build
{
public:
  Build();
  void addNewProject(Project* project);
  std::vector<std::unique_ptr<Project>> const& projects() const;
  
private:
  std::vector<std::unique_ptr<Project>> m_projects;
};

#endif
