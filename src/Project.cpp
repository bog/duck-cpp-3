#include <filesystem>
#include <Project.hpp>
#include <Parser.hpp>

Project::Project(std::string const& name)
  : m_name { name }
  , m_cxx {"g++"}
  , m_flags {""}
  , m_prefix {"/usr/local"}
  , m_objdir {"build/obj"}
  , m_bindir {"build/bin"}
{
}

std::string Project::name() const
{
  return m_name;
}

std::string Project::cxx() const
{
  return m_cxx;
}

std::string Project::flags() const
{
  return m_flags;
}

std::string Project::prefix() const
{
  return m_prefix;
}

std::string Project::objdir() const
{
  return m_objdir;
}

std::string Project::bindir() const
{
  return m_bindir;
}

std::string Project::libdir() const
{
  return m_bindir + "/../lib";
}

std::vector<std::string> const& Project::importSources() const
{
  return m_import_sources;
}


std::vector<std::string> const& Project::importHeaders() const
{
  return m_import_headers;
}

std::vector<std::string> const& Project::importLibs() const
{
  return m_import_libs;
}

std::vector<std::string> const& Project::exportBins() const
{
  return m_export_bins;
}

std::vector<std::string> const& Project::exportLibs() const
{
  return m_export_libs;
}

std::vector<std::string> const& Project::exportDirs() const
{
  return m_export_dirs;
}
