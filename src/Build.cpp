#include <Build.hpp>
#include <Project.hpp>

Build::Build()
{
}

void Build::addNewProject(Project* project)
{
  m_projects.push_back(std::move(std::unique_ptr<Project>(project)));
}


std::vector<std::unique_ptr<Project>> const& Build::projects() const
{
  return m_projects;
}
