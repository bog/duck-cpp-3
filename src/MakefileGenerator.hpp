#ifndef MAKEFILE_GENERATOR_HPP
#define MAKEFILE_GENERATOR_HPP
#include <sstream>
#include <string>

class Build;
class Project;

class MakefileGenerator
{
public:
  MakefileGenerator(Build const& build);
  std::string generate();
  void createIfNotExist(std::string const& path);
private:
  Build const& m_build;

  void createDirs(std::stringstream& makefile);
  void objGeneration(Project* project,
		     std::string const& bin_ext,
		     std::stringstream& makefile,
		     std::string const& cxx,
		     std::string const& libflag);

  void installGeneration(std::stringstream& makefile);
  void uninstallGeneration(std::stringstream& makefile);
  void mrproperGeneration(std::stringstream& makefile);
  void cleanGeneration(std::stringstream& makefile);
  void cleanDirectory(Project* project, std::stringstream& makefile, std::string const& path);

};

#endif
