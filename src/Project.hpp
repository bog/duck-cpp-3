#ifndef PROJECT_HPP
#define PROJECT_HPP
#include <string>
#include <vector>

class Parser;

class Project
{
public:
  friend class Parser;
  explicit Project(std::string const& name);

  std::string name() const;
  std::string cxx() const;
  std::string flags() const;
  std::string prefix() const;
  std::string objdir() const;
  std::string bindir() const;
  std::string libdir() const;
  
  std::vector<std::string> const& importSources() const;
  std::vector<std::string> const& importHeaders() const;
  std::vector<std::string> const& importLibs() const;
  std::vector<std::string> const& exportBins() const;
  std::vector<std::string> const& exportLibs() const;
  std::vector<std::string> const& exportDirs() const;
  
private:
  std::string m_name;
  std::string m_cxx;
  std::string m_flags;
  std::string m_prefix;
  std::string m_objdir;
  std::string m_bindir;
  
  std::vector<std::string> m_import_sources;
  std::vector<std::string> m_import_headers;
  std::vector<std::string> m_import_libs;
  std::vector<std::string> m_export_bins;
  std::vector<std::string> m_export_libs;
  std::vector<std::string> m_export_dirs;
};

#endif
